# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-sabctools
_pkgname=sabctools
pkgver=7.1.2
pkgrel=0
pkgdesc="C implementations of functions for use within SABnzbd"
url="https://github.com/sabnzbd/sabctools"
arch="all"
license="GPL-2.0-or-later AND Apache-2.0 AND CC0-1.0"
makedepends="
	linux-headers
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-chardet
	py3-jaraco.functools
	py3-portend
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sabnzbd/sabctools/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7887ff3f6b46bd538540b591ab7b47a7b3efa277b45d69ed7329252b1f7326adfa6b286cc207a4262002a802c6acff03c0df4792e099da3dd8be1c3dd68a1ae0  py3-sabctools-7.1.2.tar.gz
"
