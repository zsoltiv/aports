# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=convfmt
pkgver=0.1.5_git20230802
# https://github.com/oriontvv/convfmt/issues/76
_gitrev=f50071d2a0b2953fc0b3f0d9b7184f1342517a05
pkgrel=0
pkgdesc="CLI tool which can convert between JSON, YAML, TOML etc."
url="https://github.com/oriontvv/convfmt"
arch="all"
license="Apache-2.0"
makedepends="cargo cargo-auditable"
source="https://github.com/oriontvv/convfmt/archive/$_gitrev/convfmt-$_gitrev.tar.gz"
builddir="$srcdir/convfmt-$_gitrev"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
02fdfe045d6c67c6cdafc4a6af527177439a92e5394f1ae471ab1639a3359cf9af225cde912bd574f36f4ffe3b55747c682af5ac09c82f4920ad24c4f929280d  convfmt-f50071d2a0b2953fc0b3f0d9b7184f1342517a05.tar.gz
"
