# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mpg123
pkgver=1.32.2
pkgrel=0
pkgdesc="Console-based MPEG Audio Player for Layers 1, 2 and 3"
options="libtool"
url="https://www.mpg123.org"
arch="all"
license="LGPL-2.1-only"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc"
makedepends="libtool alsa-lib-dev linux-headers"
source="https://www.mpg123.org/download/mpg123-$pkgver.tar.bz2"

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-dependency-tracking \
		--with-pic \
		--with-optimization=0 \
		--with-cpu=i386_fpu \
		--with-audio="alsa oss"
	make
}

check() {
	make check
}

package() {
	# Installation is not parallel friendly and will fail
	# sometimes
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
08d94a0c58455e23d3d6a4aedf97775e29ae07a0e1a449d73fe018c8c6094f6db01ce368476b8d0a0d51398e215f7584aeee3ac7b84e37c866713c4dca9c01f1  mpg123-1.32.2.tar.bz2
"
