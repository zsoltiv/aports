# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-rich
pkgver=13.5.3
pkgrel=0
pkgdesc="Python library for rich text formatting and terminal formatting"
url="https://rich.readthedocs.io/en/latest/"
arch="noarch"
license="MIT"
depends="
	py3-markdown-it-py
	py3-pygments
	"
makedepends="py3-gpep517 py3-installer py3-poetry-core py3-wheel"
checkdepends="py3-pytest py3-setuptools"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/willmcgugan/rich/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/rich-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 1>&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# tests look like should pass, disabled in gentoo as well
	.testenv/bin/python3 -m pytest \
		--deselect="tests/test_card.py::test_card_render" \
		--deselect="tests/test_markdown.py::test_markdown_render" \
		--deselect="tests/test_markdown_no_hyperlinks.py::test_markdown_render" \
		--deselect="tests/test_syntax.py::test_python_render" \
		--deselect="tests/test_syntax.py::test_python_render_simple" \
		--deselect="tests/test_syntax.py::test_python_render_simple_passing_lexer_instance" \
		--deselect="tests/test_syntax.py::test_python_render_indent_guides" \
		--deselect="tests/test_syntax.py::test_option_no_wrap" \
		--deselect="tests/test_syntax.py::test_syntax_highlight_ranges"
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
acae49b796c8ff310b272bca43ce925fe717ddc6252dec20784a4730e63e9c0f81f459ee40b152c001ad0a512e47dddaadbb8fbb949ced262e2d5429b6396344  py3-rich-13.5.3.tar.gz
"
