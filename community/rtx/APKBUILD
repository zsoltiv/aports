# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.9.2
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
3e93b734a263c164b9d8678c67a4b2e5e71e14aae4e00736f1bc7f60fd4dc43de141331fd78fd6586164ca93c6deeac189fcb2868691c93ef765f619de89c181  rtx-2023.9.2.tar.gz
"
