# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=jql
pkgver=7.0.3
pkgrel=0
pkgdesc="A JSON Query Language CLI tool"
url="https://github.com/yamafaktory/jql"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable"
source="https://github.com/yamafaktory/jql/archive/jql-v$pkgver.tar.gz"
options="net" # fetch dependencies
builddir="$srcdir/jql-jql-v$pkgver"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	# core::tests::get_property_as_index seems to be broken
	cargo test --frozen -- \
		--skip core::tests::get_property_as_index
}

package() {
	install -D -m755 target/release/jql -t "$pkgdir"/usr/bin/
}

sha512sums="
e7b6d238b503470a8255177f5563bc32fa39b502366123caed5c1e89023a1e094265b251291bf81c7fd0bdcb506bb6cdd664c0b1b94cf33c91e6e33a14872842  jql-v7.0.3.tar.gz
"
