# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=docuum
pkgver=0.23.0
pkgrel=0
pkgdesc="Perform least recently used (LRU) eviction of Docker images"
url="https://github.com/stepchowfun/docuum"
# riscv64: rust is broken on this arch
arch="all !riscv64"
license="MIT"
makedepends="cargo cargo-auditable"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
source="https://github.com/stepchowfun/docuum/archive/v$pkgver/docuum-$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	$pkgname.logrotate

	getrandom-0.2.10.patch
	"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	# format::tests::code_str_display - broken test?
	cargo test --frozen -- \
		--skip format::tests::code_str_display
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/

	install -D -m755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -D -m644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
	install -D -m644 "$srcdir"/$pkgname.logrotate "$pkgdir"/etc/logrotate.d/$pkgname
}

sha512sums="
eaf4ec9f348d5eaf2a257a77003e667a12af41e402a411356eb58cb8529a3ff3d40c9172de0108b0a58e765933ed5a861a6687a68e70a47f07913c28b03885af  docuum-0.23.0.tar.gz
1bbf4baba54b4c36d44c85fa0b898e9bf10ce52ce242cc47488e8d2eb4d0ba45c6560c2ee7025811721786021ed3f4149bb7423dd1e7c07ad79c0027a5ae334d  docuum.initd
3a021f6d9fef59e4dcec56e0c91816efa7b432e4eee066888dc4dc04f08f1433b72fb48a89bbd711c96d11fde1ad5c6233b0d943ca620337c9ac955d4f95d60d  docuum.confd
63d1d053f36bb475d6775cc5806ebb44e030245a776da98724d1ac2f36247447b8d65964a8ac40d881a8c78e4224aa5f1311666e616b303a9dd879694faa0c26  docuum.logrotate
60ae9437fd926aa3ef4b47b134229a8e33f776eea266c4e7e85034a1541833a41b185cacb0cd52fcf8270ef8fb444bba680a0e0c7644e3ad7b855b141e666886  getrandom-0.2.10.patch
"
