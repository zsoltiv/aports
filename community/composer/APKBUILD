# Contributor: Nathan Johnson <nathan@nathanjohnson.info>
# Maintainer: Dave Hall <skwashd@gmail.com>
pkgname=composer
pkgver=2.6.3
pkgrel=0
pkgdesc="Dependency manager for PHP"
url="https://getcomposer.org/"
arch="noarch"
license="MIT"
_php=php82
depends="$_php $_php-phar $_php-curl $_php-iconv $_php-mbstring $_php-openssl $_php-zip"
checkdepends="git"
options="net"
source="$pkgname-$pkgver.phar::https://getcomposer.org/download/$pkgver/composer.phar"
subpackages="$pkgname-bash-completion"

# secfixes:
#   2.3.5-r0:
#     - CVE-2022-24828
#   2.1.9-r0:
#     - CVE-2021-41116
#   2.0.13-r0:
#     - CVE-2021-29472

build() {
	$_php "$srcdir"/$pkgname-$pkgver.phar completion bash > "$srcdir"/$pkgname.bash
}

check() {
	cd "$srcdir"
	$_php $pkgname-$pkgver.phar -Vn
	$_php $pkgname-$pkgver.phar -n diagnose || true # fails as pub-keys are missing
}

package() {
	install -m 0755 -D "$srcdir"/$pkgname-$pkgver.phar "$pkgdir"/usr/bin/$pkgname.phar
	printf "#!/bin/sh\n\n/usr/bin/%s /usr/bin/composer.phar \"\$@\"\n" "$_php" \
		> "$pkgdir"/usr/bin/$pkgname
	chmod +x "$pkgdir"/usr/bin/$pkgname
	install -Dm644 "$srcdir"/$pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
}

sha512sums="
244e5641e008426e21a68491f396ded6c92d632ad278310dc869a9fa9ceea21d173cf0be1b6979a78c9c2775ef09839564a77b72f676c764fb12fada9ad0780e  composer-2.6.3.phar
"
