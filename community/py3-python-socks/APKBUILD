# Maintainer: Michał Adamski <michal@ert.pl>
pkgname=py3-python-socks
pkgver=2.4.2
pkgrel=0
pkgdesc="Core proxy client (SOCKS4, SOCKS5, HTTP) functionality for Python"
options="!check" # Testsuite ships their own GLIBC version of 3proxy
url="https://github.com/romis2012/python-socks"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-async-timeout"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-curio py3-trio py3-pytest py3-yarl"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/python-socks/python-socks-$pkgver.tar.gz"
builddir="$srcdir/python-socks-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e4fdbca8794020533b1db8a735b04d95ba0161927dc49d4f58d7be1f2274e03bc479eaac4c02bcc3bb48e77f96ca4e3b130dea24ce5b147743d5f1b80cae6629  python-socks-2.4.2.tar.gz
"
