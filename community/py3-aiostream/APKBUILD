# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-aiostream
_pyname=aiostream
pkgver=0.5.0
pkgrel=0
pkgdesc="Generator-based operators for asynchronous iteration"
url="https://github.com/vxgmichel/aiostream"
arch="noarch"
license="GPL-3.0-only"
depends="python3 py3-typing-extensions"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-pytest-asyncio py3-pytest-cov"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/vxgmichel/aiostream/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}
sha512sums="
bcab27272cde42b8a09a41f9b818d8094514eeeab4b1c0304c6d2b908bcf1142d029882d51ab427b34db215dbaedf519c772be2e67a1d7f9ca5a3def0d04b7ae  aiostream-0.5.0.tar.gz
"
