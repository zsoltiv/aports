# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Maintainer: Henrik Riomar <henrik.riomar@gmail.com>
pkgname=netdata-go-plugins
pkgver=0.56.0
pkgrel=0
pkgdesc="netdata go.d.plugin"
url="https://github.com/netdata/go.d.plugin"
arch="all !x86 !armv7 !armhf" # checks fail
license="GPL-3.0-or-later"
depends="netdata"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://codeload.github.com/netdata/go.d.plugin/tar.gz/refs/tags/v$pkgver"
builddir="$srcdir/go.d.plugin-$pkgver"

export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o go.d.plugin ./cmd/godplugin
}

check() {
	go test ./...
}

package() {
	 mkdir -p "$pkgdir/usr/lib/netdata/conf.d"
	 cp -r "$builddir/config/go.d.conf" "$builddir/config/go.d" "$pkgdir/usr/lib/netdata/conf.d/"

	 mkdir -p "$pkgdir/usr/libexec/netdata/plugins.d/"
	 install -D -m755 -t "$pkgdir/usr/libexec/netdata/plugins.d" "$builddir/go.d.plugin"
}

sha512sums="
df8e1e76515dec9082a7b0edccb13f663111e36bd46c85d68c1696cecb98ddbc2aa6d77ab03e39a48060dc44e03e52fd39f1af1ed39b08c93feae62abb8093d3  netdata-go-plugins-0.56.0.tar.gz
"
